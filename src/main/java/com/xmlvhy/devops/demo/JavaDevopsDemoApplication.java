package com.xmlvhy.devops.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author mozhijun
 */
@SpringBootApplication
public class JavaDevopsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaDevopsDemoApplication.class, args);
	}

}
