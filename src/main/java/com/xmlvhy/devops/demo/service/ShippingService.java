package com.xmlvhy.devops.demo.service;

/**
 * @ClassName ShippingService
 * @Description TODO
 * @Author 小莫
 * @Date 2024/3/27 20:34
 * @Version 1.0
 **/
public class ShippingService {
    public int calculateShippingFee(int weight) {
        if (weight <= 0) {
            throw new IllegalStateException("Please provide correct weight");
        }
        if (weight <= 2) {
            return 5;
        } else if (weight <= 5) {
            return 10;
        }
        return 15;
    }
}
