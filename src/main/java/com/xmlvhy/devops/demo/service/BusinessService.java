package com.xmlvhy.devops.demo.service;

import com.xmlvhy.devops.demo.service.repository.DataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ClassName BusinessService
 * @Description TODO
 * @Author 小莫
 * @Date 2024/3/27 21:24
 * @Version 1.0
 **/
@Service
public class BusinessService {
    @Resource
    private DataService dataService;

    public int findTheGreatestFromAllData() {
        int[] data = dataService.retrieveAllData();
        int greatest = Integer.MIN_VALUE;

        for (int value : data) {
            if (value > greatest) {
                greatest = value;
            }
        }
        return greatest;
    }
}
