package com.xmlvhy.devops.demo.service.repository;

import org.springframework.stereotype.Repository;

/**
 * @ClassName DataService
 * @Description TODO
 * @Author 小莫
 * @Date 2024/3/27 21:23
 * @Version 1.0
 **/
@Repository
public class DataService {
    public int[] retrieveAllData() {
        // Some dummy data
        // Actually this should talk to some database to get all the data
        return new int[] { 1, 2, 3, 4, 5 };
    }
}
