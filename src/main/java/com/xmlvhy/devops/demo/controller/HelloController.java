package com.xmlvhy.devops.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName HelloController
 * @Description
 * @Author 代码真香
 * @Date 2022/5/28 19:56
 * @Version 1.0
 **/
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello() {

        System.out.println("/hello");

        return "Hello Java Kubesphere Devops!!!  66666668888888";
    }
}
