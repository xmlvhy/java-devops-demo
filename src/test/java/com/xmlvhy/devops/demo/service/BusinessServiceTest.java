package com.xmlvhy.devops.demo.service;

import com.xmlvhy.devops.demo.service.repository.DataService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * @Author: mozhijun
 * @Date: 2024-03-27 21:26
 * @Description:
 */
@RunWith(MockitoJUnitRunner.class)
public class BusinessServiceTest {

    @Mock
    DataService dataService;
    @InjectMocks
    BusinessService businessService;

    @Test
    public void findTheGreatestFromAllData() {
        when(dataService.retrieveAllData()).thenReturn(new int[] { 24, 15, 3 });
        assertEquals(24,businessService.findTheGreatestFromAllData());
    }
}