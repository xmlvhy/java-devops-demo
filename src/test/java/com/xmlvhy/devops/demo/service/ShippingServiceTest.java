package com.xmlvhy.devops.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @Author: mozhijun
 * @Date: 2024-03-27 20:35
 * @Description:
 */
@RunWith(MockitoJUnitRunner.class)
public class ShippingServiceTest {

    @Test
    public void incorrectWeight() {
        ShippingService shippingService = new ShippingService();
        assertThrows(IllegalStateException.class, () -> shippingService.calculateShippingFee(-1));
    }

    @Test
    public void firstRangeWeight() {
        ShippingService shippingService = new ShippingService();
        assertEquals(5, shippingService.calculateShippingFee(1));
        assertEquals(10, shippingService.calculateShippingFee(3));
        assertEquals(15, shippingService.calculateShippingFee(6));
    }
}